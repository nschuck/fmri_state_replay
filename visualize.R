

load('data/decoding_sequences.RData')
require('igraph')
require('RColorBrewer')


connectmat = c( # defines state transitions (as in the task!) 
#FyFy FyFo FyHy FyHy FoFy FoFo FoHy FoHo HyFy HyFo HyHy HyHo HoFy HoFo HoHy HoHo (Cols = TO/ Rows = FROM)
   1,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   #FyFy
   0,   0,   0,   0,   0,   0,   1,   1,   0,   0,   0,   0,   0,   0,   0,   0,   #FyFo
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   0,   0,   0,   0,   #FyHy
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   #FyHo
   0,   0,   1,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   #FoFy
   0,   0,   0,   0,   1,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   #FoFo
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   0,   0,   0,   0,   #FoHy
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   #FoHo
   1,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   #HyFy
   0,   0,   0,   0,   1,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   #HyFo
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   0,   0,   0,   0,   #HyHy
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   0,   0,   #HyHo
   1,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   #HoFy
   0,   0,   0,   0,   1,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   #HoFo
   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   0,   0,   0,   0,   0,   0,   #HoHy
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1)   #HoHo
connectmat = matrix(connectmat, ncol = 16, nrow = 16, byrow = TRUE)
colnames(connectmat) = rownames(connectmat) = statenames

# construct graph based on connectivity matrix 
g1 = graph_from_adjacency_matrix(connectmat)

# extract steps 
fsteps = distances(g1, mode = 'out') # forward steps 
fsteps_zero = fsteps # forward steps with diagonal 
fsteps[diagidx] = NaN # clean diagonal (repetitions) from standard step matrix 

HPC_fsteps = lapply(HPC, function(y) apply(y, 2, function(x) fsteps_zero[cbind(x[1:(length(x)-1)], x[2:length(x)])]))


cid = 30 
time = 3

X = HPC[[cid]][,time]

M = matrix(-1, 100, 16)

for (i in 2:99) {
	M[i,X[i]] = HPC_fsteps[[cid]][i-1,time]
}

par(mar = c(0, 0, 0, 0))
image(M, col = c('black', 'grey', brewer.pal(6, 'YlOrRd')), axes = FALSE, bty = 'n')